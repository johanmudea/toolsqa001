package co.arpsoft.setup;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SetUpWeb {

    protected WebDriver driver;
    protected ExtentReports extentReports;
    private static final String DEMO_QA_URL="https://demoqa.com/";
    private static final String INCOGNITO ="--incognito";
    private static final String HEADLESS ="--headless";

    protected  void setUpWebDriver(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments(INCOGNITO);
        //options.addArguments(HEADLESS);
        driver = new ChromeDriver(options);
    }
    protected void setUpWebdriverUrl( ){
        driver.manage().window().maximize();
        driver.get(DEMO_QA_URL);
    }

    protected  void quiteDriver(){
        driver.quit();
    }

    protected  void setUpReports(){
        extentReports = new ExtentReports();
        ExtentSparkReporter extentSparkReporter = new ExtentSparkReporter("reports/spark_report.html");
        extentReports.attachReporter(extentSparkReporter);
    }

    protected void generalSetup(){
        setUpReports();
        setUpWebDriver();
        setUpWebdriverUrl();
    }


}
