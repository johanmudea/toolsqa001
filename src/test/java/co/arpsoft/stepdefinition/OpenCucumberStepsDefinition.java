package co.arpsoft.stepdefinition;

import co.arpsoft.model.textbox.TextBoxModel;
import co.arpsoft.page.landing.LandingPage;
import co.arpsoft.page.practiceform.PracticeFormPage;
import co.arpsoft.page.shared.SharedPage;
import co.arpsoft.page.textbox.TextBoxPage;
import co.arpsoft.setup.SetUpWeb;
import com.github.javafaker.Faker;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class OpenCucumberStepsDefinition extends SetUpWeb {

    protected LandingPage landingPage;
    protected SharedPage sharedPage;
    protected PracticeFormPage practiceFormPage;
    protected TextBoxPage textBoxPage;
    protected List<String> randomCreatedUserList;
    protected TextBoxModel textBoxModel;

    private static final Logger logger = LogManager.getLogger(OpenCucumberStepsDefinition.class);

    @Given("el tester se encuentra en la landing page")
    public void elTesterSeEncuentraEnLaLandingPage() {

        try {
            generalSetup();

        }catch (Exception e){
            quiteDriver();
            logger.error("error @given: el tester se encuentra en la landing page");
        }


    }

    //PrimerScenario
    @When("el tester da click en el modulo elements")
    public void elTesterDaClickEnElModuloElements() {
        //ExtentTest testLoggers = extentReports.createTest("Logs Prueba");
        try {
            landingPage = new LandingPage(driver);
            landingPage.goToElements();
        }
        catch (Exception e){
            //testLoggers.log(Status.FAIL,"Test Fail el  assert");
            logger.error("error  @When: el tester da click en el modulo elements");
            quiteDriver();
        }


    }

    @Then("el sistema muestra una nueva pantalla")
    public void elSistemaMuestraUnaNuevaPantalla() {

        try {
            Assert.assertEquals("Elements",driver.findElement(landingPage.elementsLocatorValidator()).getText());
            quiteDriver();

        }
        catch (Exception e){
            logger.error("error   @Then: el sistema muestra una nueva pantalla");
            quiteDriver();

        }

    }


    //SegundoScenario
    @When("el tester da click en el Submodulo practiceForm")
    public void elTesterDaClickEnElSubmoduloPracticeForm() {
        try {
            landingPage = new LandingPage(driver);
            sharedPage = new SharedPage(driver);
            landingPage.goToFormsLocator();
            sharedPage.goToPracticeForm();
        }
        catch (Exception e){
            //testLoggers.log(Status.FAIL,"Test Fail el  assert");
            logger.error("error  @When: el tester da click en el Submodulo practiceForm");
            quiteDriver();
        }

    }

    @Then("ese muestra un formulario")
    public void eseMuestraUnFormulario() {

        try {
            practiceFormPage = new PracticeFormPage(driver);
            Assert.assertEquals("Student Registration Form", driver.findElement(practiceFormPage.getStudentRegistationFormTittle()).getText());
            quiteDriver();
        }
        catch (Exception e){
            logger.error("error @Then: ese muestra un formulario");
            quiteDriver();

        }

    }

    //TercerScenario
    @When("el tester da click en el Submodulo elements->TextBox y lo diligencia")
    public void elTesterDaClickEnElSubmoduloElementsTextBoxYLoDiligencia() {
        try {
            generateUserFaker();
            landingPage = new LandingPage(driver);
            sharedPage = new SharedPage(driver);
            textBoxPage = new TextBoxPage(driver,textBoxModel);
            landingPage.goToElements();
            sharedPage.goToTextBoxInsideElementsMenu();
            //randomCreatedUserList = textBoxPage.generateUser();
            textBoxPage.fillTextBoxFills();
        }
        catch (Exception e){
            //testLoggers.log(Status.FAIL,"Test Fail el  assert");
            logger.error("error   @When(el tester da click en el Submodulo elements->TextBox y lo diligencia");
            quiteDriver();
        }

    }
    @Then("el sistema los datos de confirmación.")
    public void elSistemaLosDatosDeConfirmación() {
        try {
            textBoxPage = new TextBoxPage(driver, textBoxModel);
            Assert.assertEquals(textBoxModel.getFullName(),driver.findElement(textBoxPage.getFullNameAssertionLocator()).getText());

            //Assert.assertEquals("Name:"+randomCreatedUserList.get(0),driver.findElement(textBoxPage.getFullNameAssertionLocator()).getText());
            //Assert.assertEquals("Email:"+randomCreatedUserList.get(1),driver.findElement(textBoxPage.getEmailAssertionLocator()).getText());
            //Assert.assertEquals("Current Address :"+randomCreatedUserList.get(2),driver.findElement(textBoxPage.getCurrentAddressAssertionLocator()).getText());
            //Assert.assertEquals("Permananet Address :"+randomCreatedUserList.get(3),driver.findElement(textBoxPage.getPermanentAddressAssertionLocator()).getText());
            quiteDriver();
        }

        catch (Exception e){
            logger.error("error @Then el sistema los datos de confirmación.");
            quiteDriver();

        }

    }

    //Funciones de ayuda

    private void generateUserFaker(){
        textBoxModel = new TextBoxModel();
        Faker faker = new Faker();
        String fakerFullName = faker.name().fullName();
        String fakerEmail = faker.internet().emailAddress();
        String fakerAddress = faker.address().fullAddress();
        textBoxModel.setFullName(fakerFullName);
        textBoxModel.setEmail(fakerEmail);
        textBoxModel.setCurrentAddress(fakerAddress);
        textBoxModel.setPermanentAdress(fakerAddress);
    }

    public List<String> forSubmittedFormUserFaker(){
        List<String> submitedFormResult = new ArrayList<>();
        submitedFormResult.add(textBoxModel.getFullName());
        submitedFormResult.add(textBoxModel.getEmail());
        submitedFormResult.add(textBoxModel.getCurrentAddress());
        submitedFormResult.add(textBoxModel.getPermanentAdress());
        return submitedFormResult;
    }

}
