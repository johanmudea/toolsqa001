package co.arpsoft.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/open_landing_page.feature"},
        glue = {"co.arpsoft.stepdefinition"},
        tags = "@test003")
public class OpenCucumberRunner {

}
