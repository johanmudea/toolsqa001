package co.arpsoft.runner;

import co.arpsoft.page.landing.LandingPage;
import co.arpsoft.setup.SetUpWeb;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class OpenLandingPageRunner  extends SetUpWeb {

    private final By elementsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[1]/div/div[3]/h5");
    private final By formsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[2]/div/div[3]/h5");
    private final By alertsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[3]/div/div[3]/h5");
    private final By widgetsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[4]/div/div[3]/h5");
    private final By interactionsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[5]/div/div[3]/h5");
    private final By bookStoreApp= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[6]/div/div[3]/h5");

    private final By elementsInsidePageLocator= By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[1]/span/div/div[1]");

    private static final Logger logger = LogManager.getLogger(OpenLandingPageRunner.class);
    private LandingPage landingPage;

    //private WebDriver driver;
    //private ExtentReports extentReports;


    @Before
    public void setUp(){
        //extentReports = new ExtentReports();
        //WebDriverManager.chromedriver().setup();
        //ChromeOptions options = new ChromeOptions();
        //options.addArguments("--incognito");
        //driver = new ChromeDriver(options);
        //driver.manage().window().maximize();
        //driver.get("https://demoqa.com/");

        generalSetup();
        logger.info("Se ha configurado el General Setur");

    }

    @Test
    public void testRunner() throws InterruptedException {

        ExtentTest testLoggers = extentReports.createTest("Logs Prueba");

        try {
            //driver.findElement(elementsLocator).click();
             landingPage = new LandingPage(driver);
            landingPage.goToElements();
            Assert.assertEquals("Elements",driver.findElement(elementsInsidePageLocator).getText());
            testLoggers.log(Status.PASS,"Test Fail el  assert");


        }

        catch (Exception e){
            testLoggers.log(Status.FAIL,"Test Fail el  assert");
            logger.error("este es un error de que entro al bloque cath del @test");
            driver.quit();

        }


    }

    @After
    public void quitDriver(){
        quiteDriver();
        extentReports.flush();
    }

}
