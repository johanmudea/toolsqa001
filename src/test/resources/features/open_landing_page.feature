Feature: Protect autonomization
  como tester deseo automatizar diferentes flujos
  de las herramientas suministradas en la pagina
  de tools QA.

  Background:
    Given el tester se encuentra en la landing page
  @test001
  Scenario: Ingress a módulo de elements
    When el tester da click en el modulo elements
    Then  el sistema muestra una nueva pantalla

  @test002
  Scenario: Ingress a módulo de Forms
    When el tester da click en el Submodulo practiceForm
    Then  ese muestra un formulario

  @test003
  Scenario: Ingress a Submodulo de  de elements->TextBox y lo diligencia
    When el tester da click en el Submodulo elements->TextBox y lo diligencia
    Then  el sistema los datos de confirmación.
