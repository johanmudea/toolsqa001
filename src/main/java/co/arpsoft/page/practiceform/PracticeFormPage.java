package co.arpsoft.page.practiceform;

import co.arpsoft.page.base.CommonBasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PracticeFormPage extends CommonBasePage {



    public PracticeFormPage(WebDriver driver) {
        super(driver);
    }

    private  final By studentRegistationFormTittleLocator = By.xpath("//*[@id=\"app\"]/div/div/div/div[2]/div[2]/h5");

    public By getStudentRegistationFormTittle() {

        return studentRegistationFormTittleLocator;
    }

}


