package co.arpsoft.page.shared;

import co.arpsoft.page.base.CommonBasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SharedPage extends CommonBasePage {



    public SharedPage(WebDriver driver) {
        super(driver);
    }



    //Locators On the sharedPage
    private final By elementsMenuLocator = By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[1]/span/div/div[1]");
    private final By formsMenuLocator = By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[2]/span/div/div[1]");
    private final By alertsMenuLocator= By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[3]/span/div/div[1]");
    private final By widgetsMenuLocator= By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[4]/span/div/div[1]");
    private final By interactionsMenuLocator= By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[5]/span/div/div[1]");
    private final By bookStoreAppMenuLocator= By.xpath("//*[@id=\"app\"]/div/div/div/div[1]/div/div/div[6]/span/div/div[1]");

    private final By practiceFormsInsideFormsMenu= By.xpath("//span[contains(@class,'text')and text()='Practice Form']");
    private final By textBoxInsideElementsMenu= By.xpath("//span[contains(@class,'text')and text()='Text Box']");



    public void goToPracticeForm(){

        click(formsMenuLocator);
        click(practiceFormsInsideFormsMenu);

    }

    public void goToTextBoxInsideElementsMenu(){

        //click(elementsMenuLocator);
        click(textBoxInsideElementsMenu);

    }


    public By getElementsMenuLocator() {
        return elementsMenuLocator;
    }

}
