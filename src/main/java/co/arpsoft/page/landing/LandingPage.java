package co.arpsoft.page.landing;

import co.arpsoft.page.base.CommonBasePage;
import co.arpsoft.page.shared.SharedPage;
import com.sun.istack.internal.XMLStreamReaderToContentHandler;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingPage extends CommonBasePage {

    private WebDriver driver;

    public LandingPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //Locators On the LandingPAge
    private final By elementsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[1]/div/div[3]/h5");
    private final By formsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[2]/div/div[3]/h5");
    private final By alertsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[3]/div/div[3]/h5");
    private final By widgetsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[4]/div/div[3]/h5");
    private final By interactionsLocator= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[5]/div/div[3]/h5");
    private final By bookStoreApp= By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div[6]/div/div[3]/h5");

    //Acciones
    public void goToElements(){
        click(elementsLocator);
    }
    public void goToFormsLocator(){
        click(formsLocator);
    }
    public void goToAlertsLocator(){
        click(alertsLocator);
    }
    public void goToWidgetsLocator(){
        click(widgetsLocator);
    }
    public void goToInteractionsLocator(){
        click(interactionsLocator);
    }
    public void goToBookStoreApp(){
        click(bookStoreApp);
    }

    public By elementsLocatorValidator(){
        SharedPage sharedPage = new SharedPage(driver);
        return sharedPage.getElementsMenuLocator();
    }

}
