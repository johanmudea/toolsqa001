package co.arpsoft.page.textbox;

import co.arpsoft.model.textbox.TextBoxModel;
import co.arpsoft.page.base.CommonBasePage;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class TextBoxPage extends CommonBasePage {

    private final TextBoxModel textBoxModel;

    public TextBoxPage(WebDriver driver, TextBoxModel textBoxModel) {
        super(driver);
        this.textBoxModel = textBoxModel;
    }

    //localizadores del formulario
    private final By fullNameLocator = By.xpath("//*[@id=\"userName\"]");
    private final By emailLocator = By.xpath("//input[contains(@class,'mr-sm-2 form-control') and contains(@placeholder,'name@example.com')]");
    private final By currentAddressLocator = By.xpath("//*[@id='currentAddress']");
    private final By permanentAddressLocator = By.xpath("//*[@id='permanentAddress']");
    private final By submitButton = By.id("submit") ;


    //localizadoresde la assertion
    /*
    private final By fullNameAssertionLocator = By.xpath("//*[@id='name']/text()[2]");
    private final By emailAssertionLocator = By.xpath("//*[@id='email']/text()[2]");
    private final By currentAddressAssertionLocator = By.xpath("//*[@id='currentAddress']/text()[2]");
    private final By permanentAddressAssertionLocator = By.xpath("//*[@id='permanentAddress']/text()[2]");
    */
    //localizadoresde la assertionModificadosQutandoleLasParinal
    private final By fullNameAssertionLocator = By.xpath("//*[@id='name']");
    private final By emailAssertionLocator = By.xpath("//*[@id='email']");
    private final By currentAddressAssertionLocator = By.xpath("//*[@id='currentAddress']");
    private final By permanentAddressAssertionLocator = By.xpath("//*[@id='permanentAddress']");


    public By getFullNameAssertionLocator() {
        return fullNameAssertionLocator;
    }


    public By getPermanentAddressAssertionLocator() {
        return permanentAddressAssertionLocator;
    }

    public By getCurrentAddressAssertionLocator() {
        return currentAddressAssertionLocator;
    }

    public By getEmailAssertionLocator() {
        return emailAssertionLocator;
    }

    //funcionalidades
    public List<String> generateUser(){

        Faker faker = new Faker();
        String randomfullName = faker.name().fullName();
        String randomemailAddress = faker.internet().emailAddress();
        String randomSameAddress = faker.address().fullAddress();

        List<String> randomCreatedUserList = new ArrayList<>();
        randomCreatedUserList.add(randomfullName.trim());
        randomCreatedUserList.add(randomemailAddress.trim());
        randomCreatedUserList.add(randomSameAddress.trim());
        randomCreatedUserList.add(randomSameAddress.trim());

        return randomCreatedUserList;
    }
    public void fillTextBoxFills(){

        //String randomFullName = randomCreatedUserList.get(0);
        //String randomEmailAddress = randomCreatedUserList.get(1);
        //String randomSameAddress = randomCreatedUserList.get(2);

        typeInto(fullNameLocator,textBoxModel.getFullName());
        typeInto(emailLocator,textBoxModel.getEmail());
        typeInto(currentAddressLocator,textBoxModel.getCurrentAddress());
        typeInto(permanentAddressLocator,textBoxModel.getPermanentAdress());
        scroll(submitButton);
        click(submitButton);

    }


    public List<String> isFilledDone(){
        List<String> submitedFormResultAssertion = new ArrayList<>();
        submitedFormResultAssertion.add(getText(fullNameAssertionLocator).trim());
        submitedFormResultAssertion.add(getText(emailAssertionLocator).trim());
        submitedFormResultAssertion.add(getText(currentAddressAssertionLocator).trim());
        submitedFormResultAssertion.add(getText(permanentAddressAssertionLocator).trim());
        return submitedFormResultAssertion;
    }



}
