package co.arpsoft.page.base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class CommonBasePage {

    private final WebDriver driver;

    public CommonBasePage(WebDriver driver) {
        this.driver = driver;

    }

    protected void typeInto(By locator, String value){
        driver.findElement(locator).sendKeys(value);
    }

    protected void clearText(By locator){
        driver.findElement(locator).clear();
    }

    protected void click(By locator){
        driver.findElement(locator).click();
    }

    protected String getText(By locator){
        return driver.findElement(locator).getText();
    }

    protected void scroll(By locator){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
    }


}
